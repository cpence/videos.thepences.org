
function initApp() {
  shaka.polyfill.installAll();
  if (shaka.Player.isBrowserSupported()) {
    initPlayer();
  } else {
    $('#unsupported').show();
  }
}

function initPlayer() {
  var video = document.getElementById('video');
  var player = new shaka.Player(video);

  window.player = player;
  player.addEventListener('error', onErrorEvent);

  var manifestUri = $('#video').data('manifest');
  player.load(manifestUri).then(function() {
    console.log('The video has now been loaded!');
  }).catch(onError);
}

function onErrorEvent(event) {
  onError(event.detail);
}

function onError(error) {
  // FIXME: think about some way to display these?
  console.error('Error code', error.code, 'object', error);
}

$(function() {
  initApp();
})
