const glob = require("glob");
const path = require("path");
const moment = require("moment");
const md = require("markdown-it")({ linkify: true, typographer: true });

// Load all of the video metadata from the given input folder
exports.getAll = function (grunt, input) {
  var filenames = glob.sync(path.join(input, "**", "*_meta.json"));
  var metadata = filenames.map((f) => grunt.file.readJSON(f));

  // Custom sort on these for later
  metadata.sort(function (a, b) {
    // First by category
    if (a.category < b.category) {
      return -1;
    } else if (a.category > b.category) {
      return 1;
    }

    // Then by date, descending
    if (a.date < b.date) {
      return 1;
    } else if (a.date > b.date) {
      return -1;
    }

    // Then by title
    if (a.title < b.title) {
      return -1;
    } else if (a.title > b.title) {
      return 1;
    }

    return 0;
  });

  return metadata;
};

// Group the metadata by category
exports.groupByCategory = function (grunt, metadata) {
  var ret = {};
  metadata.forEach(function (m) {
    if (ret[m.category] === undefined) {
      ret[m.category] = [];
    }
    ret[m.category].push(m);
  });

  return ret;
};

// Build the list of Pug templates to process from the input folder
exports.pugFileList = function (grunt, input) {
  var filenames = glob.sync(path.join(input, "**", "*_meta.json"));

  var outputs = filenames.map(function (fn) {
    var rel = path.relative(input, fn);
    var base = path.basename(rel, "_meta.json");

    return path.join("_build", path.dirname(rel), base + ".html");
  });

  var ret = {};
  outputs.forEach((fn) => (ret[fn] = "templates/video.pug"));

  return ret;
};

// Get the metadata for a particular file (for Pug's data function)
exports.forFile = function (grunt, input, dest, src) {
  var dest_base = path.basename(dest, ".html");
  // The slice(6) here takes off '_build/'
  var json_file = path.join(
    input,
    path.dirname(dest).slice(7),
    dest_base + "_meta.json"
  );

  var data = grunt.file.readJSON(json_file);

  // Reformat the dates
  data.date = moment(data.date).format("MMMM Do YYYY, h:mm a");

  // Reformat the descriptions (Markdown)
  data.description = md.render(data.description);

  return { ...data, ...exports.common(grunt, input) };
};

// Get the metadata for all categories
exports.forCategories = function (grunt, input) {
  var filenames = glob.sync(path.join(input, "*", "_metadata.json"));
  var metadata = filenames.map((f) => grunt.file.readJSON(f));

  var ret = {};
  metadata.forEach(function (m) {
    ret[m.id] = m;
  });

  return ret;
};

// Build the list of Pug templates to process into category roots
exports.pugCategoryList = function (grunt, input) {
  var categories = exports.forCategories(grunt, input);
  var ret = {};

  for (const [id, list] of Object.entries(categories)) {
    ret[path.join("_build", id, "index.html")] = "templates/category.pug";
  }

  return ret;
};

// Get the metadata for a particular category (for Pug's data function)
exports.forCategory = function (grunt, input, dest, src) {
  // The slice(6) here takes off '_build/'
  var category_id = path.dirname(dest).slice(7);

  var categories = exports.forCategories(grunt, input);
  var grouped = exports.groupByCategory(grunt, exports.getAll(grunt, input));

  return {
    id: category_id,
    category: categories[category_id],
    list: grouped[category_id],
    ...exports.common(grunt, input),
  };
};

// Get the site-wide metadata
exports.forSite = function (grunt, input) {
  return grunt.file.readJSON(path.join(input, "_metadata.json"));
};

// Get common metadata that should be used on every template
exports.common = function (grunt, input) {
  return {
    site: exports.forSite(grunt, input),
    categories: exports.forCategories(grunt, input),
  };
};
