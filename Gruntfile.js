const sass = require("sass");
const untildify = require("untildify");
const metadata = require("./metadata");

module.exports = function (grunt) {
  var input = untildify(grunt.option("input") || ".");
  var all_metadata = metadata.getAll(grunt, input);

  // Configure our Grunt tasks
  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),

    clean: ["_build"],

    sass: {
      options: {
        implementation: sass,
        sourceMap: undefined,
        outputStyle: "compressed",
        includePaths: ["node_modules/bootstrap/scss"],
      },
      dist: {
        files: {
          "_build/css/main.css": "css/main.scss",
        },
      },
    },

    uglify: {
      main: {
        files: {
          "_build/js/main.min.js": [
            "node_modules/jquery/dist/jquery.js",
            "node_modules/bootstrap/dist/js/bootstrap.bundle.js",
          ],
        },
      },
      video: {
        files: {
          "_build/js/video.min.js": [
            "node_modules/shaka-player/dist/shaka-player.compiled.js",
            "js/video.js",
          ],
        },
      },
    },

    pug: {
      videos: {
        options: {
          data: metadata.forFile.bind(this, grunt, input),
        },
        files: metadata.pugFileList(grunt, input),
      },
      categories: {
        options: {
          data: metadata.forCategory.bind(this, grunt, input),
        },
        files: metadata.pugCategoryList(grunt, input),
      },
      index: {
        options: {
          data: {
            videos: all_metadata,
            videos_category: metadata.groupByCategory(grunt, all_metadata),
            ...metadata.common(grunt, input),
          },
        },
        files: {
          "_build/index.html": "templates/index.pug",
        },
      },
    },

    symlink: {
      videos: {
        files: [
          {
            expand: true,
            overwrite: false,
            cwd: input,
            src: ["**/*.webm", "**/*.mpd", "**/*.png"],
            dest: "_build",
          },
        ],
      },
    },

    run: {
      serve: {
        cmd: "yarn",
        args: ["run", "serve", "--symlinks", "_build"],
      },
    },
  });

  grunt.loadNpmTasks("grunt-contrib-clean");
  grunt.loadNpmTasks("grunt-sass");
  grunt.loadNpmTasks("grunt-contrib-uglify");
  grunt.loadNpmTasks("grunt-contrib-pug");
  grunt.loadNpmTasks("grunt-contrib-symlink");
  grunt.loadNpmTasks("grunt-run");

  // Note that we do not run the symlink tasks by default, you can do that
  // yourself if you need it for debugging
  grunt.registerTask("default", ["sass", "uglify", "pug"]);
  grunt.registerTask("serve", ["default", "symlink", "run:serve"]);
};
